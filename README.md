# README #

Test automation project of [IA Financial Group](http://ia.ca).

To see test results for mortgage price calculation at [IA Financial Group](http://ia.ca) site
please follow the next steps:
1. Download sources.
2. Open command line.
3. Switch to project directory.
4. Execute 'mvn clean install' command.
5. After execution generated test report can be found at: 
    project directory/target/surefire-reports/index.html .