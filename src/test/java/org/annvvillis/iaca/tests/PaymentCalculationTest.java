package org.annvvillis.iaca.tests;

import org.annvvillis.iaca.DriversFactory;
import org.annvvillis.iaca.Page;
import org.annvvillis.iaca.PagesFactory;
import org.annvvillis.iaca.element.ListElement;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class PaymentCalculationTest {

    private WebDriver driver;
    private WebDriverWait driverWait;

    private Page homePage;
    private Page mortgagePage;
    private Page paymentCalculatorPage;

    @BeforeClass
    public void init() {
        driver = DriversFactory.getDriver("FirefoxDriver");
        driverWait = new WebDriverWait(driver, 20, 1000);

        homePage = PagesFactory.getPage(driver, "HomePage");
        mortgagePage = PagesFactory.getPage(driver, "MortgagePage");
        paymentCalculatorPage = PagesFactory.getPage(driver, "PaymentCalculatorPage");
    }

    @AfterClass
    public void tearDown() {
        if (driver != null)
            driver.quit();
    }

    @Test
    public void testHomePageIsOpened() {
        // Step 1
        homePage.open();
        driverWait.until(ExpectedConditions.urlToBe(homePage.getUrl()));
        Assert.assertEquals(driver.getCurrentUrl(), homePage.getUrl(),
                "'Home' page wasn't opened:");
    }

    @Test(dependsOnMethods = {"testHomePageIsOpened"})
    public void testLoansPopupIsOpened() {
        // Step 2
        homePage.getElement("loansElement").click();
        driverWait.until(ExpectedConditions.visibilityOf(homePage.getElement("mortgageLink")));
        Assert.assertTrue(homePage.getElement("mortgageLink").isDisplayed(),
                "'Loans' pop-up wasn't opened:");
    }

    @Test(dependsOnMethods = {"testLoansPopupIsOpened"})
    public void testMortgagePageIsOpened() {
        // Step 3
        if (!homePage.getElement("mortgageLink").isDisplayed())
            homePage.getElement("loansElement").click();
        homePage.getElement("mortgageLink").click();
        driverWait.until(ExpectedConditions.urlToBe(mortgagePage.getUrl()));
        Assert.assertEquals(driver.getCurrentUrl(), mortgagePage.getUrl(),
                "'Mortgage' page wasn't opened:");
    }

    @Test(dependsOnMethods = {"testMortgagePageIsOpened"})
    public void testCalculatePaymentBtnLeadsToCalculatePage() {
        // Step 4
        mortgagePage.getElement("calculatePaymentsButton").click();
        driverWait.until(ExpectedConditions.urlToBe(paymentCalculatorPage.getUrl()));
        Assert.assertEquals(driver.getCurrentUrl(), paymentCalculatorPage.getUrl(),
                "'Calculate mortgage payment' page wasn't opened:");
    }

    @Test(dependsOnMethods = {"testCalculatePaymentBtnLeadsToCalculatePage"})
    public void testPurchasePriceSlider() {
        // Steps 5-6
        movePurchasePriceSlider(paymentCalculatorPage);

        // Assert slider for maximum value
        String priceValue = paymentCalculatorPage.getElement("hiddenPriceSlider").getAttribute("value");
        Assert.assertEquals(priceValue, "2000000",
                "'Purchase price' slider wasn't moved as expected:");

        // Assert slider position
        String sliderPercent = paymentCalculatorPage.getElement("priceSlider").getAttribute("style");
        Assert.assertTrue(sliderPercent.contains("left: 100"),
                "'Purchase price' slider wasn't moved as expected:");

        slidePurchasePriceValue(paymentCalculatorPage, "0");
    }

    @Test(dependsOnMethods = {"testPurchasePriceSlider"})
    public void testMortgagePriceCalculation() {
        // Step 7
        slidePurchasePriceValue(paymentCalculatorPage, "500000");

        // Step 8
        typeDownPaymentValue(paymentCalculatorPage, "50000");

        // Step 9
        ListElement amortizationList = new ListElement(paymentCalculatorPage.getElement("amortizationList"));
        amortizationList.selectValue("15");

        // Step 10
        ListElement paymentFrequencyList = new ListElement(paymentCalculatorPage.getElement("paymentFrequencyList"));
        paymentFrequencyList.selectValue("weekly");

        // Step 11
        typeInterestRate(paymentCalculatorPage, "5");

        // Step 12
        paymentCalculatorPage.getElement("calculateBtn").click();

        // Step 13
        String actualPrice = paymentCalculatorPage.getElement("resultsLabel").getText();
        Assert.assertEquals(actualPrice, "$ 836.75", "Calculation of mortgage payment wasn't correct:");
    }

    private void scrollToElement(WebElement element) {
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
    }

    private void slidePurchasePriceValue(Page page, String expectedValue) {
        WebElement purchasePriceInputElement = page.getElement("purchasePriceInput");
        scrollToElement(purchasePriceInputElement);
        purchasePriceInputElement.clear();
        while (!expectedValue.equals(purchasePriceInputElement.getAttribute("value")))
            page.getElement("purchasePricePlusBtn").click();
    }

    private void typeDownPaymentValue(Page page, String value) {
        WebElement downPaymentInputElement = page.getElement("downPaymentInput");
        scrollToElement(downPaymentInputElement);
        downPaymentInputElement.sendKeys(value);
        downPaymentInputElement.sendKeys(Keys.ENTER);
    }

    private void typeInterestRate(Page page, String value) {
        WebElement interestRateInputElement = page.getElement("interestRateInput");
        scrollToElement(interestRateInputElement);
        interestRateInputElement.clear();
        interestRateInputElement.sendKeys(value);
        interestRateInputElement.sendKeys(Keys.ENTER);
    }

    private void movePurchasePriceSlider(Page page) {
        // Get slider width
        WebElement priceSliderHorizontal = page.getElement("priceSliderHorizontal");
        scrollToElement(priceSliderHorizontal);
        Dimension sliderSize = priceSliderHorizontal.getSize();
        int sliderWidth = sliderSize.getWidth();

        // Get slider x coordinate
        WebElement priceSlider = page.getElement("priceSlider");
        int xCoord = priceSlider.getLocation().getX();

        // Drag slider
        Actions builder = new Actions(driver);
        builder.moveToElement(priceSlider)
                .click()
                .dragAndDropBy(priceSlider, xCoord + sliderWidth, 0)
                .build()
                .perform();
    }

}
