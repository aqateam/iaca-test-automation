package org.annvvillis.iaca;

import org.annvvillis.iaca.page.HomePage;
import org.annvvillis.iaca.page.MortgagePage;
import org.annvvillis.iaca.page.PaymentCalculatorPage;
import org.openqa.selenium.WebDriver;

public class PagesFactory {

    public static Page getPage(WebDriver driver, String pageName) {
        if ("HomePage".equalsIgnoreCase(pageName))
            return new HomePage(driver);
        else if ("MortgagePage".equalsIgnoreCase(pageName))
            return new MortgagePage(driver);
        else if ("PaymentCalculatorPage".equalsIgnoreCase(pageName))
            return new PaymentCalculatorPage(driver);
        else
            throw new RuntimeException("Not valid page name. Allowed values: "
                    + "'HomePage', 'MortgagePage' or 'PaymentCalculatorPage'.");
    }

}
