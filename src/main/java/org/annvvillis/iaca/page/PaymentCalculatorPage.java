package org.annvvillis.iaca.page;

import org.annvvillis.iaca.Page;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class PaymentCalculatorPage implements Page {

    private final static String PAGE_URL = "https://ia.ca/mortgage-payment-calculator";

    @FindBy(how = How.ID, using = "PrixPropriete")
    private WebElement purchasePriceInput;

    @FindBy(how = How.ID, using = "PrixProprietePlus")
    private WebElement purchasePricePlusBtn;

    @FindBy(how = How.XPATH, using = "//div[@class='slider-handle min-slider-handle custom']")
    private WebElement priceSlider;

    @FindBy(how = How.XPATH, using = "//div[@class='slider slider-horizontal']")
    private WebElement priceSliderHorizontal;

    @FindBy(how = How.ID, using = "sliderPrixPropriete")
    private WebElement hiddenPriceSlider;

    @FindBy(how = How.ID, using = "MiseDeFond")
    private WebElement downPaymentInput;

    @FindBy(how = How.XPATH, using = "//label[@for='Amortissement']/following-sibling::div")
    private WebElement amortizationList;

    @FindBy(how = How.XPATH, using = "//label[@for='FrequenceVersement']/following-sibling::div")
    private WebElement paymentFrequencyList;

    @FindBy(how = How.ID, using = "TauxInteret")
    private WebElement interestRateInput;

    @FindBy(how = How.ID, using = "btn_calculer")
    private WebElement calculateBtn;

    @FindBy(how = How.XPATH, using = "//span[@id='paiement-resultats']")
    private WebElement resultsLabel;

    private WebDriver driver;

    public PaymentCalculatorPage(final WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @Override
    public String getUrl() {
        return PAGE_URL;
    }

    @Override
    public void open() {
        driver.get(PAGE_URL);
        driver.manage().window().maximize();
    }

    @Override
    public WebElement getElement(String elementName) {
        switch (elementName) {
            case "purchasePriceInput":
                return purchasePriceInput;
            case "purchasePricePlusBtn":
                return purchasePricePlusBtn;
            case "priceSlider":
                return priceSlider;
            case "priceSliderHorizontal":
                return priceSliderHorizontal;
            case "hiddenPriceSlider":
                return hiddenPriceSlider;
            case "downPaymentInput":
                return downPaymentInput;
            case "amortizationList":
                return amortizationList;
            case "paymentFrequencyList":
                return paymentFrequencyList;
            case "interestRateInput":
                return interestRateInput;
            case "calculateBtn":
                return calculateBtn;
            case "resultsLabel":
                return resultsLabel;
            default:
                throw new NoSuchElementException(String.format("Element '%s' not found.", elementName));
        }
    }

}
