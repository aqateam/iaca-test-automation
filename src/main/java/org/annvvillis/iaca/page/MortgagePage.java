package org.annvvillis.iaca.page;

import org.annvvillis.iaca.Page;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class MortgagePage implements Page {

    private final static String PAGE_URL = "http://ia.ca/mortgage";

    @FindBy(how = How.XPATH, using = "//a[contains(.,'Calculate your payments')]")
    private WebElement calculatePaymentsButton;

    private final WebDriver driver;

    public MortgagePage(final WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @Override
    public String getUrl() {
        return PAGE_URL;
    }

    @Override
    public void open() {
        driver.get(PAGE_URL);
        driver.manage().window().maximize();
    }

    @Override
    public WebElement getElement(String elementName) {
        switch (elementName) {
            case "calculatePaymentsButton":
                return calculatePaymentsButton;
            default:
                throw new NoSuchElementException(String.format("Element '%s' not found.", elementName));
        }
    }

}
