package org.annvvillis.iaca;

import org.openqa.selenium.WebElement;

public interface Page {

    String getUrl();

    void open();

    WebElement getElement(String elementName);

}
