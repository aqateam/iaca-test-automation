package org.annvvillis.iaca;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;

public class DriversFactory {

    public static WebDriver getDriver(String driveName) {
        if ("FirefoxDriver".equalsIgnoreCase(driveName)) {
            System.setProperty("webdriver.gecko.driver", "./src/main/resources/geckodriver.exe");
            FirefoxOptions options = new FirefoxOptions();
            options.addArguments("--private");
            return new FirefoxDriver(options);
        } else if ("ChromeDriver".equalsIgnoreCase(driveName)) {
            System.setProperty("webdriver.chrome.driver", "./src/main/resources/chromedriver.exe");
            ChromeOptions options = new ChromeOptions();
            options.addArguments("--incognito");
            return new ChromeDriver(options);
        } else {
            throw new RuntimeException("Not valid driver name. Allowed values: "
                    + "'FirefoxDriver' or 'ChromeDriver'.");
        }
    }

}
