package org.annvvillis.iaca.element;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

public class ListElement {

    private final WebElement element;

    public ListElement(WebElement element) {
        this.element = element;
    }

    public void selectValue(String text) {
        element.click();
        element.sendKeys(text);
        element.sendKeys(Keys.ENTER);
    }

    public String getSelectedValue(){
        return element.getAttribute("value");
    }

}
